﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IntegratedDigitalLogisticsSkillTest
{
    public static class PasswordHelper
    {
        public static string HashPlainTextPassword(string password)
        {
            var md5 = new MD5CryptoServiceProvider();
            string hashedPassword = Encoding.ASCII.GetString(md5.ComputeHash(Encoding.ASCII.GetBytes(password)));
            return hashedPassword;
        }
    }
}
