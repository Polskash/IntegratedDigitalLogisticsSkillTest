﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IntegratedDigitalLogisticsSkillTest.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace IntegratedDigitalLogisticsSkillTest.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            return View("Login");
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel inputModel)
        {
            if (!ModelState.IsValid)
                return View(inputModel);

            if (!await IsAuthenticUser(inputModel.Email, inputModel.Password))
            {
                ModelState.AddModelError(String.Empty, "Invalid username or password.");
                return View(inputModel);
            }

            await LoginUser(inputModel.Email);

            return RedirectToAction("Index", "Home");
        }

        private async Task LoginUser(string email)
        {
            // create claims
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "IDL Secure User"),
                new Claim(ClaimTypes.Email, email)
            };

            // create identity
            ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

            // create principal
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            // sign-in
            await HttpContext.SignInAsync(
                    scheme: "IDLSecurityScheme",
                    principal: principal);

            Thread.CurrentPrincipal = principal;
        }

        private async Task<bool> IsAuthenticUser(string email, string password)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
            {
                string hashedPassword = PasswordHelper.HashPlainTextPassword(password);

                try
                {
                    connection.Open();

                    //check if email already exists
                    SqlCommand cmd = new SqlCommand("SELECT COUNT(UserProfileId) FROM UserProfile WHERE Email = @email AND Password = @hashedPassword", connection);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@hashedPassword", hashedPassword);

                    int count = (int)await cmd.ExecuteScalarAsync();

                    return count > 0;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
            }

            return false;
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            string hashedPassword = PasswordHelper.HashPlainTextPassword(model.Password);

            using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
            {
                try
                {
                    //check if email already exists
                    SqlCommand cmd = new SqlCommand("SELECT COUNT(UserProfileId) FROM UserProfile WHERE Email = @email", connection);
                    cmd.Parameters.AddWithValue("@email", model.Email);

                    connection.Open();

                    int count = (int)await cmd.ExecuteScalarAsync();

                    if (count > 0)
                    {
                        ModelState.AddModelError("Email", "That email is already registered.");
                        return View(model);
                    }

                    //insert the new user
                    cmd = new SqlCommand("INSERT INTO UserProfile (Email, Password, FirstName, LastName, DateOfBirth, Address, City, State, ZipCode, PersonalInfo) VALUES (@email, @hashedPassword, @firstName, @lastName, @dateOfBirth, @address, @city, @state, @zip, @personalInfo)", connection);
                    cmd.Parameters.AddWithValue("@email", model.Email);
                    cmd.Parameters.AddWithValue("@hashedPassword", hashedPassword);
                    cmd.Parameters.AddWithValue("@firstName", model.FirstName);
                    cmd.Parameters.AddWithValue("@lastName", model.LastName);
                    cmd.Parameters.AddWithValue("@dateOfBirth", model.DateOfBirth);
                    cmd.Parameters.AddWithValue("@address", model.Address);
                    cmd.Parameters.AddWithValue("@city", model.City);
                    cmd.Parameters.AddWithValue("@state", model.State);
                    cmd.Parameters.AddWithValue("@zip", model.Zip);
                    cmd.Parameters.AddWithValue("@personalInfo", model.PersonalInfo);

                    await cmd.ExecuteNonQueryAsync();

                    //log the user in
                    await LoginUser(model.Email);
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(
                    scheme: "IDLSecurityScheme");

            return RedirectToAction("Login");
        }
    }
}