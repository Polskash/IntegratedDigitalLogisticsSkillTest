﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IntegratedDigitalLogisticsSkillTest.Models;
using Microsoft.AspNetCore.Authorization;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Threading;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;

namespace IntegratedDigitalLogisticsSkillTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            this._hostingEnvironment = hostingEnvironment;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            string email = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();

            HomeModel model = new HomeModel();
            model.Email = email;

            using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
            {
                conn.Open();

                string sql = @"SELECT FirstName, LastName, DateOfBirth, Address, City, State, ZipCode, PersonalInfo FROM UserProfile where email = @email";

                using (SqlCommand comm = new SqlCommand(sql, conn))
                {
                    comm.Parameters.AddWithValue("@email", email);

                    var reader = await comm.ExecuteReaderAsync();

                    while (reader.Read())
                    {
                        model.FirstName = reader["FirstName"] as string;
                        model.LastName = reader["LastName"] as string;
                        model.DateOfBirth = (reader["DateOfBirth"] as DateTime?).HasValue ? (reader["DateOfBirth"] as DateTime?).Value : DateTime.Today;
                        model.Address = reader["Address"] as string;
                        model.City = reader["City"] as string;
                        model.State = reader["State"] as string;
                        model.Zip = reader["ZipCode"] as string;
                        model.PersonalInfo = reader["PersonalInfo"] as string;
                    }
                }

                model.Videos = await LoadVideosForUser();
            }
            
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> UpdateUser(HomeModel model)
        {
            if (!ModelState.IsValid)
                return View("Index", model);

            model.Email = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();

            //update user data
            using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Update UserProfile " +
                        "                            Set FirstName = @firstName, LastName = @lastName, DateOfBirth = @dateOfBirth, Address = @address, " +
                        "                            City = @city, State = @state, ZipCode = @zip, PersonalInfo = @personalInfo " +
                                                    "WHERE Email = @email", connection);

                    cmd.Parameters.AddWithValue("@email", model.Email);
                    cmd.Parameters.AddWithValue("@firstName", model.FirstName);
                    cmd.Parameters.AddWithValue("@lastName", model.LastName);
                    cmd.Parameters.AddWithValue("@dateOfBirth", model.DateOfBirth);
                    cmd.Parameters.AddWithValue("@address", model.Address);
                    cmd.Parameters.AddWithValue("@city", model.City);
                    cmd.Parameters.AddWithValue("@state", model.State);
                    cmd.Parameters.AddWithValue("@zip", model.Zip);
                    cmd.Parameters.AddWithValue("@personalInfo", model.PersonalInfo);

                    connection.Open();
                    await cmd.ExecuteNonQueryAsync();

                    TempData["UpdateUserResult"] = true;
                }
                catch(Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
            }

            return View("Index", model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize]
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<JsonResult> UploadFile(IFormFile file)
        {
            List<VideoModel> videos = new List<VideoModel>();
            try
            {
                string writeDirectory = $"{this._hostingEnvironment.WebRootPath}\\uploaded_files";

                string filePathOfUploadedVideo = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName;

                filePathOfUploadedVideo = filePathOfUploadedVideo.Trim('"');

                string fileName = Path.GetFileName(filePathOfUploadedVideo);

                string fullFilePath = Path.Combine(writeDirectory, fileName);

                using (var stream = new FileStream(fullFilePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                //insert a db record for the uploaded video
                string email = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();

                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    try
                    {
                        connection.Open();

                        SqlCommand cmd = new SqlCommand("INSERT INTO Video (Email, Filename) VALUES (@email, @filename)", connection);
                        cmd.Parameters.AddWithValue("@email", email);
                        cmd.Parameters.AddWithValue("@filename", fileName);

                        await cmd.ExecuteNonQueryAsync();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                //Thread.Sleep(2000);

                //load up the video files for this user
                videos = await LoadVideosForUser();
            }
            catch (Exception ex)
            {

            }

            //return the list of videos that this user has uploaded
            return new JsonResult(videos);
        }

        private async Task<List<VideoModel>> LoadVideosForUser()
        {
            List<VideoModel> videos = new List<VideoModel>();

            string email = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();

            using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
            {
                conn.Open();

                string sql = @"SELECT FileName FROM Video where email = @email";

                using (SqlCommand comm = new SqlCommand(sql, conn))
                {
                    comm.Parameters.AddWithValue("@email", email);

                    var reader = await comm.ExecuteReaderAsync();

                    while (reader.Read())
                    {
                        VideoModel videoModel = new VideoModel() { Filename = reader["FileName"] as string };
                        videoModel.DownloadURL = "/uploaded_files/" + videoModel.Filename;
                        videos.Add(videoModel);
                    }
                }
            }

            return videos;
        }
    }
}
