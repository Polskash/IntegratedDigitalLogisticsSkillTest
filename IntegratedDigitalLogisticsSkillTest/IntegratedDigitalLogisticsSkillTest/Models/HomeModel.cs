﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IntegratedDigitalLogisticsSkillTest.Models
{
    public class HomeModel
    {
        public List<VideoModel> Videos { get; set; } = new List<VideoModel>();

        public string Email { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]        
        [Display(Name = "Date of Birth")]
        public DateTime? DateOfBirth { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        [Display(Name = "Personal Info")]
        public string PersonalInfo { get; set; }
    }
}
