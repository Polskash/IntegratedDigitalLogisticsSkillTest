﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegratedDigitalLogisticsSkillTest.Models
{
    public class VideoModel
    {
        public string Filename { get; set; }
        public string DownloadURL { get; set; }
    }
}
